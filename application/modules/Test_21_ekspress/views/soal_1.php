<html lang="en" style="height: auto;">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <!-- <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css"> -->
    <!-- iCheck -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/summernote/summernote-bs4.min.css">
    <!-- jQuery -->
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/jquery/jquery.min.js"></script>
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/jquery/jquery.redirect.js"></script>
    <!-- Bootstrap 4 -->
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables -->
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <!-- Select2 -->
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="http://localhost/codeigniter_hmvc/assets/admin_lte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

</head>

<body class="sidebar-mini layout-fixed sidebar-closed sidebar-collapse" style="height: auto; padding:16px">
    <form method="post" target="_blank" action="<?=$action?>">
        <input type="text" name="kata" autocomplete="off">
        <button type="submit">Submit</button>
    </form>
</body>

</html>