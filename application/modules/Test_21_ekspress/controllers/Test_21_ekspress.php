<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test_21_ekspress extends MX_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(['ion_auth', 'form_validation']);
		$this->load->helper(['url', 'language']);

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	public function index()
	{
		$this->data['action'] =base_url('test_21_ekspress/check_kata');
		$this->load->view('soal_1', $this->data);
	}

	public function check_kata()
	{
		$kata = $_POST['kata'];
		$kata_len = strlen($kata);
		$tampung_kata = "";
		for ($i=$kata_len-1; $i >= 0 ; $i--) {
			$tampung_kata .= substr($kata,$i,1);
		}
		$kata = strtoupper($kata);
		$tampung_kata = strtoupper($tampung_kata);
		$output = "Output Menghasilkan ";
		$juga = "";
		$status  = "Bukan";
		if($kata == $tampung_kata){
			$status  = "Ya";
			$juga = "juga";
		}
		echo "Output menghasilkan '$status' karena kata '$kata' dibalik $juga menjadi '$tampung_kata'";
	}

	public function soal_2()
	{
		$this->data['action'] =base_url('test_21_ekspress/check_soal_2');
		$this->load->view('soal_1', $this->data);
	}

	public function check_soal_2()
	{
		$kata = $_POST['kata'];
		$arr_kata  = explode(" ",$kata);
		$arr_kata_len =  count($arr_kata);
		$tampung_kata = $arr_kata[0];
		for ($i=1; $i < $arr_kata_len ; $i++) { 
			$tampung_kata .= ucfirst($arr_kata[$i]);
		}
		echo $tampung_kata;
	}

	public function soal_3()
	{
		$this->data['action'] =base_url('test_21_ekspress/check_soal_3');
		$this->load->view('soal_1', $this->data);
	}

	public function check_soal_3()
	{
		$kata = $_POST['kata'];
		$arr_kata  = explode(",",$kata);
		$arr_kata_len =  count($arr_kata);
		$count_positif = 0;
		$count_negatif = 0;
		$count_nol = 0;
		$tampung_kata = $arr_kata[0];
		for ($i=0; $i < $arr_kata_len ; $i++) {
			
			if ($arr_kata[$i] > 0) {
				$count_positif++;
			}elseif ($arr_kata[$i] == 0) {
				$count_nol++;
			}elseif ($arr_kata[$i] < 0) {
				$count_negatif++;
			}
		}
		$rasio_positif = $count_positif/$arr_kata_len;
		$rasio_negatif = $count_negatif/$arr_kata_len;
		$rasio_nol = $count_nol/$arr_kata_len;
		echo number_format($rasio_positif,5)."<br>";
		echo number_format($rasio_negatif,5)."<br>";
		echo number_format($rasio_nol,5)."<br>";
	}

	public function soal_4()
	{
		$this->data['action'] =base_url('test_21_ekspress/check_soal_4');
		$this->load->view('soal_1', $this->data);
	}

	public function check_soal_4()
	{
		$alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',];
		$len = $_POST['kata'];
		for ($i=$len-1; $i >=0  ; $i--) { 
			// echo $alphabet[$i]."<br>";
			for ($x=$i; $x >=0  ; $x--) { 
				echo $alphabet[$x];
			}
			echo "<br>";
		}		
		for ($i=0; $i < $len ; $i++) { 
			$temp = $len;
			for ($i=0; $i < $temp; $i++) { 
				# code...
			}
			# code...
		}
	}

}
